## ☀️ Primer Test

+++

## Primer Test

- En el directorio `.\cypress\integration` crea un archivo con nombre `primerTest.js` 
- Luego abrimos Cypress con `npm run cy:open` usando el script que hemos creado en `package.json` 
- En la pantalla que se abre seleccionamos el archivo que hemos creado que todavía no tiene contenido con el botón `Open in IDE`

+++ 

<img src="./img/primerTest.png" alt="primer test" width="80%"/>

+++ 

- Puede que nos pida permiso para saltarse el firewall de windows
- Escribe el contenido siguiente en el archivo recién creado

```javascript
describe('My First Test', () => {
  it('Visits the Kitchen Sink', () => {
    cy.visit('https://example.cypress.io')
  })
})
```

- Sin necesidad de guardar los cambios en el archivo, Cypress se pone a ejecutar el test recién escrito
- Cuando queramos cerrar Cypress solo tenemos que usar control + C en la consola donde antes hemos lanzado `npm run cy:open`

+++

<video data-autoplay width="100%" controls>
  <source src="./videos/primerTest.mp4" type="video/mp4" >
</video>

+++

- Añadimos la siguiente línea

```javascript
cy.contains('type').click()
```

![test correcto](./img/testCorrecto.png)

- Como se puede ver todo ha salido bien

+++

- Modificamos la línea para que falle el test cambiando `type` por `tipe` que es un elemento que no esta en la página

![test correcto](./img/testFallido.png)

- Se muestra el error y donde se ha producido

+++

- Añadimos este script en el archivo package.json

```json
 "cy:run": "cypress run  --spec  cypress\\integration\\primerTest.js"
```

- Esto nos permite lanzar el test creado anteriormente desde la línea de comandos con `npm run cy:run`

+++

- Esto es lo que sale por la consola con el test correcto.

```bash
> Cypress run  --spec  cypress\integration\primerTest.js

====================================================================================================

  (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:    7.6.0                                                                              │
  │ Browser:    Electron 89 (headless)                                                             │
  │ Specs:      1 found (primerTest.js)                                                            │
  │ Searched:   cypress\integration\primerTest.js                                                  │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


────────────────────────────────────────────────────────────────────────────────────────────────────

  Running:  primerTest.js                                                                   (1 of 1)


  My First Test
    √ Visits the Kitchen Sink (3260ms)


  1 passing (5s)


  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        1                                                                                │
  │ Passing:      1                                                                                │
  │ Failing:      0                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  0                                                                                │
  │ Video:        true                                                                             │
  │ Duration:     5 seconds                                                                        │
  │ Spec Ran:     primerTest.js                                                                    │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


  (Video)

  -  Started processing:  Compressing to 32 CRF
  -  Finished processing: C:\proyectos\cypress\primerproyectocypress\cypress\videos\p    (2 seconds)
                          rimerTest.js.mp4

====================================================================================================

  (Run Finished)


       Spec                                              Tests  Passing  Failing  Pending  Skipped  
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ √  primerTest.js                            00:05        1        1        -        -        - │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
    √  All specs passed!                        00:05        1        1        -        -        -  

```

+++

- Esto es lo que sale por la consola con el test fallido.

```bash
> Cypress run  --spec  cypress\integration\primerTest.js

====================================================================================================

  (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:    7.6.0                                                                              │
  │ Browser:    Electron 89 (headless)                                                             │
  │ Specs:      1 found (primerTest.js)                                                            │
  │ Searched:   cypress\integration\primerTest.js                                                  │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘

────────────────────────────────────────────────────────────────────────────────────────────────────

  Running:  primerTest.js                                                                   (1 of 1)


  My First Test
    1) Visits the Kitchen Sink


  0 passing (8s)
  1 failing

  1) My First Test
       Visits the Kitchen Sink:
     AssertionError: Timed out retrying after 4000ms: Expected to find content: 'tipe' but never did.
      at Context.eval (https://example.cypress.io/__cypress/tests?p=cypress\integration\primerTest.js:102:8)

  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        1                                                                                │
  │ Passing:      0                                                                                │
  │ Failing:      1                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  1                                                                                │
  │ Video:        true                                                                             │
  │ Duration:     7 seconds                                                                        │
  │ Spec Ran:     primerTest.js                                                                    │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


  (Screenshots)

  -  C:\proyectos\cypress\primerproyectocypress\cypress\screenshots\primerTest.js\My     (1920x1080)
     First Test -- Visits the Kitchen Sink (failed).png


  (Video)

  -  Started processing:  Compressing to 32 CRF
  -  Finished processing: C:\proyectos\cypress\primerproyectocypress\cypress\videos\p    (2 seconds)
                          rimerTest.js.mp4

====================================================================================================

  (Run Finished)


       Spec                                              Tests  Passing  Failing  Pending  Skipped  
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ ×  primerTest.js                            00:07        1        -        1        -        - │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
    ×  1 of 1 failed (100%)                     00:07        1        -        1        -        -  
```

+++

- La información que da es bastante clara
- A destacar que lanzando el test con la opción `run` se graba un video del test que podemos ver en el directorio `videos` y , en el caso del test fallido, se puede ver una captura de pantalla del fallo en el directorio `screenshots`
- También se puede ver que ha lanzado el test con el navegador Electron 89 en modo no gráfico lo que no impide que haga las capturas de los errores y el vídeo de forma correcta
- Esto se puede cambiar como veremos más adelante

+++

Captura del fallo **My First Test -- Visits the Kitchen Sink (failed)**

![My First Test -- Visits the Kitchen Sink (failed)](./img/failed.png)

+++

- Video del test correcto

<video data-autoplay width="100%" controls>
  <source src="./videos/primerTest.js.mp4" type="video/mp4" >
</video>

+++ 

⬅️ [instalación](?p=00-instalacion)
🏠 [inicio](?)
➡️ [uso durante desarrollo](?p=02-uso-durante-desarrollo)
