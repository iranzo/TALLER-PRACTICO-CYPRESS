# Taller práctico de cypress

* [Cypress](https://www.cypress.io/) una herramienta para automatizar las pruebas e2e sin utilizar Selenium
    * e2e son esas pruebas que pretenden simular el comportamiento de un usuario real<!-- .element: class="fragment" -->
* Se puede usar tanto durante el desarrollo del programa como para los test finales en los diferentes entornos de despliegue<!-- .element: class="fragment" -->

+++

Enlace a: 
* [instalación](?p=00-instalacion), 
* [primer test](?p=01-primer-test),
* [uso durante desarrollo](?p=02-uso-durante-desarrollo), 
* [uso en pre producción](?p=03-uso-en-preproduccion), 
* [test visual](?p=04-test-visual), 
* [final](?p=end)

--- 

* Repositorio del [taller-practico-cypress en gitlab](https://gitlab.com/iranzo/taller-practico-cypress)
* Repositorio del [ejemplo primerproyectocypress](https://gitlab.com/iranzo/primerproyectocypress)