## ☀️ Uso en preproducción

+++

* Tenemos que probar la modificación realizada en la pantalla de alta de candidatos del colectivo 5 sin destino definitivo al acto de elección de funcionarios 
* Hemos añadido tres nuevos campos Año ingreso , Orden ingreso y Nota ingreso <!-- .element: class="fragment" -->
* los tres campos sólo deben admitir números <!-- .element: class="fragment" -->

+++

Este es el test que hemos usado en desarrollo local

```javascript
describe('test desarrollo', () => {
    it('primer test', () => {
        cy.visit('localhost:8080/epacf_adm/start.action?idUsuario=jiranzo&idAplicacion=24&token=1')
        /* ==== Generated with Cypress Studio ==== */
        cy.log('**AÑADIR CANDIDATO**')
        // AÑADIR CANDIDATO
        cy.get(':nth-child(2) > .textoApl1 > a').click();
        cy.get(':nth-child(8) > a > .sinBorde').click();
        cy.get(':nth-child(6) > a').click();
        cy.contains('Añadir Candidato').click();
        cy.get('#actProcessAgnadirActoCandidatoColectivo_idColectivo').select('4');
        cy.get('#actProcessAgnadirActoCandidatoColectivo_nif').clear();
        cy.get('#actProcessAgnadirActoCandidatoColectivo_nif').type('17703071V');
        cy.get('#Aceptar').click();
        cy.contains('Añadir Candidato - Colectivo 4 - 5 - Sin destino definitivo')
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_actoCandidatoColectivo4_gestor_id').select('50');
        // comprobando el año de ingreso
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_actoCandidatoColectivo4_agnoIngreso').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        ('El año de ingreso debe ser un valor numérico')
        cy.contains('El año de ingreso debe ser un valor numérico')
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_actoCandidatoColectivo4_agnoIngreso').clear().type('1960')
        // comprobando el orden de ingreso
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_actoCandidatoColectivo4_ordenIngreso').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        ('El orden de ingreso debe ser un valor numérico')
        cy.contains('El orden de ingreso debe ser un valor numérico')
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_actoCandidatoColectivo4_ordenIngreso').clear().type('1234')
        // comprobando la nota de ingreso
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_notaIngreso').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        ('La nota de ingreso no es válida')
        cy.contains('La nota de ingreso no es válida')
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_notaIngreso').clear().type('1.2345')
        // comprobando la puntuación del concurso de traslados
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_puntuacion').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        ('La puntuación introducida no es válida')
        cy.contains('La puntuación introducida no es válida')
        cy.get('#actProcessAgnadirActoCandidatoColectivo4_puntuacion').clear().type('5.4321')
        cy.get('#Aceptar').click();
        cy.log('**MODIFICAR CANDIDATO**')
        // MODIFICAR CANDIDATO  
        cy.get('#actProcessBuscarActoCandidatosColectivo_formBusqueda_nif').clear().type('17703071V');
        cy.get('#actProcessBuscarActoCandidatosColectivo_formBusqueda_idColectivo').select('4');
        cy.get('#Buscar').click();
        cy.get(':nth-child(10) > a > .sinBorde').click();
        cy.contains('Modificar Candidato - Colectivo 4 - 5 - Sin destino definitivo')
        // comprobando el año de ingreso
        cy.get('#actProcessModificarActoCandidatoColectivo4_actoCandidatoColectivo4_agnoIngreso').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        ('El año de ingreso debe ser un valor numérico')
        cy.contains('El año de ingreso debe ser un valor numérico')
        cy.get('#actProcessModificarActoCandidatoColectivo4_actoCandidatoColectivo4_agnoIngreso').clear().type('1960')
        // comprobando el orden de ingreso
        cy.get('#actProcessModificarActoCandidatoColectivo4_actoCandidatoColectivo4_ordenIngreso').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        ('El orden de ingreso debe ser un valor numérico')
        cy.contains('El orden de ingreso debe ser un valor numérico')
        cy.get('#actProcessModificarActoCandidatoColectivo4_actoCandidatoColectivo4_ordenIngreso').clear().type('1234')
        // comprobando la nota de ingreso
        cy.get('#actProcessModificarActoCandidatoColectivo4_notaIngreso').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.get('.errorMessage').scrollIntoView()
        ('La nota de ingreso no es válida')
        cy.contains('La nota de ingreso no es válida')
        cy.get('#actProcessModificarActoCandidatoColectivo4_notaIngreso').clear().type('6.789')
        // comprobando la puntuación del concurso de traslados
        cy.get('#actProcessModificarActoCandidatoColectivo4_puntuacion').clear().type('JAVIER')
        cy.get('#Aceptar').click();
        cy.contains('La puntuación introducida no es válida')
        cy.get('#actProcessModificarActoCandidatoColectivo4_puntuacion').clear().type('0.1234')
        cy.get('#Aceptar').click();

        cy.log('**BORRAR EL CANDIDATO**')
        // BORRAR EL CANDIDATO
        cy.get('#actProcessBuscarActoCandidatosColectivo_formBusqueda_nif').clear().type('17703071V');
        cy.get('#Buscar').click();
        cy.get('#actProcessBuscarActoCandidatosColectivo_formBusqueda_idColectivo').select('4');
        cy.get('#Buscar').click();
        cy.get(':nth-child(12) > a > .sinBorde').click();
        cy.get('#Aceptar').click();
        cy.get('.actionMessage').click();
        cy.contains('El candidato se ha eliminado correctamente')
        /* ==== End Cypress Studio ==== */
    })
})
```

+++

* Lo hemos usado para añadir un candidato, para modificarlo y borrarlo todo esto contra nuestro servidor en local
* Ahora tenemos que hacer esas mismas comprobaciones en los entornos de desarrollo y pre producción<!-- .element: class="fragment" -->
* Vamos a realizar el ejemplo en pre producción<!-- .element: class="fragment" -->
* Comprobaremos también que la versión del programa que estamos probando es la correcta y hacemos una captura del elemento que tiene la versión<!-- .element: class="fragment" -->

+++

* Este es el test que usariamos para comprobar la versión durante el desarrollo en local

```javascript
    it('La versión del programa', () => {
        cy.visit('localhost:8080/epacf_adm/start.action?idUsuario=jiranzo&idAplicacion=24&token=1')
        // captura de la versión del programa
        cy.contains('Versión 1.0.13').screenshot('version');
    })
```
+++

* Y este es el test que tenemos que usar para comprobar la versión en pre producción donde tenemos que pasar por el login en GEB

```javascript
    it('La versión del programa', () => {
        cy.visit('https://preaplicaciones.aragon.es/geb/');
        cy.get('#processLogin_idUsuario').type('jiranzo');
        cy.get('#processLogin_password').type('test');
        cy.contains('Conectar').click();
        cy.contains('EPACF_ADM - Actos de Elección de Funcionarios').should('exist').click();
        // captura de la versión del programa
        cy.contains('Versión 1.0.13').screenshot('version');
    })
```
+++

* Para poder reutilizar los tests, conviene separarlos en test distintos
* Por eso tenemos un test para la versión y otro para la comprobación de la modificación
* Esto nos llevaria a repetir la parte del código donde hacemos el login con GEB
* Para evitar este repetición de código podemos usar los [hooks](https://docs.cypress.io/guides/references/best-practices#Using-after-or-afterEach-hooks)

```javascript
  beforeEach(() => {
        cy.visit('https://preaplicaciones.aragon.es/geb/')
        cy.get('#processLogin_idUsuario').type('jiranzo');
        cy.get('#processLogin_password').type('test');
        cy.contains('Conectar').click();
        cy.contains('EPACF_ADM - Actos de Elección de Funcionarios').should('exist').click();
      })
    // compruebo la versión del programa con la versión que tiene que tener
    it('La versión del programa', () => {
        // captura de la versión del programa
        cy.contains('Versión 1.0.13').screenshot('version');
    })
```    

+++ 

* El código que hemos puesto en el `hook beforeEach` se repite antes de todo los test
* Cypress proporciona hooks [para antes y despues de todos los test y para antes y despues de cada uno de ellos](https://docs.cypress.io/guides/core-concepts/writing-and-organizing-tests#Hooks)

```javascript
    // al finalizar cada uno de los tests
    afterEach(() => {
        cy.get('a[href*="logout.action"]').click()
    })
```    

* Para lanzar el test podemos añadir esta línea a nuestro archivo `package.json`

```json
    "cy:run-pre": "cypress run  --spec  cypress\\integration\\pre-produccion.js"
```

* Para lanzarlo con `npm run cy:run-pre`

+++

- Video del test 

<video data-autoplay width="100%" controls>
  <source src="./videos/pre-produccion.js.mp4" type="video/mp4" >
</video>

+++ 

⬅️ [uso durante desarrollo](?p=02-uso-durante-desarrollo)
🏠 [inicio](?)
➡️ [test visual](?p=04-test-visual)
