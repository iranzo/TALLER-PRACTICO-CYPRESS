## ☀️ Uso durante desarrollo

+++

* Tenemos que desarrollar una pantalla con un formulario y unos campos que no pueden estar vacíos y deben ser numéricos
* Lanzamos Cypress con `npm run open`

+++


![test correcto](./img/nuevoSpecFile.png)

Seleccionamos New Spec File

+++

* Guardamos el nuevo archivo en `C:\Proyectos\cypress\primerproyectocypress\cypress\integration` con el nombre `desarrollo.js`
* Cypress ha creado el archivo con este contenido

```javascript
// desarrollo.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test
```

+++

Seleccionamos el nuevo archivo creado y usamos el botón Open in IDE

![abrir en ide](./img/abrirEnIde.png)

+++ 

Y escribimos el siguiente código que nos permitirá acceder a la pantalla inicial de desarrollo

```javascript
describe('test desarrollo' , () => {
    it('primer test', () => {
        cy.visit('localhost:8080/eppla_adm/start.action?idUsuario=jiranzo&idAplicacion=54&token=1')
    })
})
```

Por supuesto tenemos que tener la aplicación que estamos desarrollando en marcha

+++

Para usar la utilidad `experimental studio` tenemos que añadirla en el archivo `cypress.json`

```json
{ 
  "viewportWidth": 1300,
  "viewportHeight": 660,
  "experimentalStudio": true 
}
```

Esto nos permite añadir comandos al test de forma interactiva y de paso definimos el tamaño de la pantalla del navegador

![agnadir Commandos Al Test](./img/agnadirCommandosAlTest.png)

Las acciones que hacemos en la pantalla se van guardando y al final con `Save commands` se pueden guardar

+++ 

Este es el código que ha generado Cypress en mi caso

```javascript
    /* ==== Generated with Cypress Studio ==== */
    cy.get('[d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"]').click({ force: true });
    cy.get('#plaProcessBuscarPlantillaCentros_formBusqueda_codCentro').clear();
    cy.get('#plaProcessBuscarPlantillaCentros_formBusqueda_codCentro').type('50000126');
    cy.get('#Buscar').click();
    cy.get('[title="Modificar centro"] > .feather-padding > .feather').click();
    cy.get(':nth-child(1) > :nth-child(11) > [title="Modificar especialidad del centro"] > .feather-padding > .feather').click();
    cy.get(':nth-child(13) > .container-fluid > :nth-child(1) > .card-body > .row').click();
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaJuridicaCursoActual').clear();
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaJuridicaCursoActual').type('JAVIER');
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionJubilacion').clear();
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionJubilacion').type('JAVIER');
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaNegativa').clear();
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaNegativa').type('JAVIER');
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaVacante').clear();
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaVacante').type('JAVIER');
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaCentroEspecialidad_observaciones').clear();
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaCentroEspecialidad_observaciones').type('OBSERVACIONES DE LA PLANTILLA DEL CURSO 2021 PARA EL CENTRO 50000126 Y LA ESPECIALIDAD 0597 - AL CAMBIADAS');
    cy.get('#Aceptar').click();
    /* ==== End Cypress Studio ==== */
```

+++ 

* He añadido la opción `{ force: true }` al primer comando para evitar un fallo por estar el elemento cubierto por otro
* Vemos que han saltado las validaciones que hemos programado para los cuatro campos en los que se puede modificar los datos
* Añadimos esta comprobación en el test

```javascript
    // validando los campos con formato no válido
    cy.contains('El campo Plantilla Jurídica Curso Próximo tiene un formato no válido').should('exist');
    cy.contains('El campo Ocupación Jubilación tiene un formato no válido').should('exist');
    cy.contains('El campo Ocupación Reserva Negativa tiene un formato no válido').should('exist');
    cy.contains('El campo Ocupación Reserva Vacante tiene un formato no válido').should('exist');
```
* Y vemos que automáticamente se vuelve a ejecutar el test 

![comprobación](./img/comprobacion.png)

* Con las validaciones pasadas

+++

* Añadimos la comprobación del requisito que impide que esos campos esten vacios

```javascript
   // validando los campos con valor núlo
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaJuridicaCursoActual').clear();
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionJubilacion').clear();
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaNegativa').clear();
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaVacante').clear();

    cy.get('#Aceptar').click();

    cy.log('**PROBANDO LA VALIDACIÓN DE LOS CAMPOS DESDE LAS PROPIEDADES DEL HTML**')

    cy.get('#plaProcessModificarPlantillaCentroEspecialidad').within(() => {
      cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaJuridicaCursoActual').invoke('prop', 'validationMessage')
        .should('equal', 'Completa este campo')
    })

    cy.get('#plaProcessModificarPlantillaCentroEspecialidad').within(() => {
      cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionJubilacion').invoke('prop', 'validationMessage')
        .should('equal', 'Completa este campo')
    })
    cy.get('#plaProcessModificarPlantillaCentroEspecialidad').within(() => {
      cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaNegativa').invoke('prop', 'validationMessage')
        .should('equal', 'Completa este campo')
    })

    cy.get('#plaProcessModificarPlantillaCentroEspecialidad').within(() => {
      cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaVacante').invoke('prop', 'validationMessage')
        .should('equal', 'Completa este campo')
    })

```

+++

* Esta comprobación es un poco especial dado que se usa la propiedad `required` de html y no los errores que da el servidor

+++


* Luego probamos con valores válidos y comprobamos la correcta grabación de esos datos

```javascript
  // valores validos
  cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaJuridicaCursoActual').clear().type('20');
  cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionJubilacion').clear().type('40');
  cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaNegativa').clear().type('50');
  cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaVacante').clear().type('60');
  cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaCentroEspecialidad_observaciones').clear().type('OBSERVACIONES DE LA PLANTILLA DEL CURSO 2021 PARA EL CENTRO 50000126 Y LA ESPECIALIDAD 0597 - AL CAMBIADAS');
  cy.get('#Aceptar').click();
  // comprobando la grabación
  cy.contains('La operación se ha realizado correctamente').should('exist')
  // comprobando los valores introducidos
  cy.get(':nth-child(1) > :nth-child(11) > [title="Modificar especialidad del centro"]').click({ force: true });
  cy.get('#plaProcessModificarPlantillaCentroEspecialidad_plantillaJuridicaCursoActual').invoke('val').should('eq', '20');
  cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionJubilacion').invoke('val').should('eq', '40');
  cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaNegativa').invoke('val').should('eq', '50');
  cy.get('#plaProcessModificarPlantillaCentroEspecialidad_ocupacionReservaVacante').invoke('val').should('eq', '60');
```

+++

- Video del test correcto 

<video data-autoplay width="100%" controls>
  <source src="./videos/desarrollo.mp4" type="video/mp4" >
</video>

+++ 

⬅️ [uso durante desarrollo](?p=02-uso-durante-desarrollo)
🏠 [inicio](?)
➡️ [uso en preproducción](?p=03-uso-en-preproduccion)
