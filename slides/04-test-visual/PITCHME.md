## ☀️ Test visual

+++

* También se puede comprobar el aspecto visual de la aplicación
* Para eso tenemos que instalar el plugin [cypress-image-snapshot](https://www.npmjs.com/package/cypress-image-snapshot)

```bash
npm install --save-dev cypress-image-snapshot
```

* Esto modifica el archivo package.json

```js
"devDependencies": {
"cypress": "^7.6.0",
"cypress-image-snapshot": "^4.0.1"
}
```

+++ 

* Una vez instalado el plugin tenemos que añadir al final de este archivo  `C:\proyectos\primerproyectocypress\cypress\plugins\index.js` el siguiente código

```javascript
const {
  addMatchImageSnapshotPlugin,
} = require('cypress-image-snapshot/plugin');

module.exports = (on, config) => {
  addMatchImageSnapshotPlugin(on, config);
};
```

* Y en `C:\proyectos\primerproyectocypress\cypress\support\commands.js`

```javascript
import { addMatchImageSnapshotCommand } from 'cypress-image-snapshot/command';

addMatchImageSnapshotCommand();
```

* Esto nos permite añadir en nuestro test esta instrucción

```javascript
cy.get(<elemento>).matchImageSnapshot()
```   
+++ 

* El código del test visual

```javascript
describe('test visual', () =>
{
    it('comprobando el escudo de forma visual', () => {
        cy.visit('..//pruebas/paddoc.html')
        cy.get('.alto56').matchImageSnapshot()

    })

    it('comprobando el logo de forma visual', () => {
        cy.visit('..//pruebas/paddoc.html')
    
        cy.get('img').matchImageSnapshot()

    })
})
```

+++ 

* El test se lanza sobre un archivo html en local que es la descarga de la pantalla de inicio de PADDOC 
* Vamos a comprobar si el escudo y el logo de esa pantalla inicial se mantienen iguales

* Añadimos este código en package.json para lanzarlo desde la consola

```json
    "cy:run-visual": "cypress run --spec  cypress\\integration\\visual-testing.js"
```

+++ 

* La primera vez que se lanza el test se crea el directorio `C:\proyectos\cypress\primerproyectocypress\cypress\snapshots\visual-testing.js` donde se almacenan las imágenes con las que se hace la comprobación

![escudo](./img/test_visual_comprobando_el_escudo_de_forma_visual.snap.png)
![logo](./img/test_visual_comprobando_el_logo_de_forma_visual.snap.png)

+++ 

* Cambio el archivo logoInternet.jpg por el archivo en blanco y vuelvo a lanzar el test
* Cypresss comprueba las diferencias entre las capturas guardadas y la actual y si hay diferencias las guarda en el directorio `__diff_output__` con nombre `test visual -- comprobando el escudo de forma visual.diff.png` donde se ve la captura inicial, la actual y las diferencias

![diferencias](./img/diff_output/test_visual_comprobando_el_escudo_de_forma_visual.diff.png)

+++ 


* En el test fallido sale la información del porcentaje de fallo y del número de pixels diferentes

```bash
  test visual
    1) comprobando el escudo de forma visual
    √ comprobando el logo de forma visual (2703ms)


  1 passing (19s)
  1 failing

  1) test visual
       comprobando el escudo de forma visual:
     Error: Image was 13.047952825990208% different from saved snapshot with 9382 different pixels.
See diff for details: C:\proyectos\cypress\primerproyectocypress\cypress\snapshots\visual-testing.js\__diff_output__\test visual -- comprobando el escudo de forma visual.diff.png
      at Context.eval (http://localhost:53622/__cypress/tests?p=cypress\support\index.js:202:17)


  (Screenshots)

  -  C:\proyectos\cypress\primerproyectocypress\cypress\snapshots\visual-testing.js\_      (1284x56)
     _diff_output__\test visual -- comprobando el escudo de forma visual.diff.png
  -  C:\proyectos\cypress\primerproyectocypress\cypress\screenshots\visual-testing.js    (1920x1080)
     \test visual -- comprobando el escudo de forma visual (failed).png
  -  C:\proyectos\cypress\primerproyectocypress\cypress\snapshots\visual-testing.js\t      (330x310)
     est visual -- comprobando el logo de forma visual.snap.png


  (Video)

  -  Started processing:  Compressing to 32 CRF
  -  Finished processing: C:\proyectos\cypress\primerproyectocypress\cypress\videos\v    (6 seconds)
                          isual-testing.js.mp4

```

+++ 


⬅️ [uso en pre producción](?p=03-uso-en-preproduccion)
🏠 [inicio](?)
➡️ [final](?p=end)
