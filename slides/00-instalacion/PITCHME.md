## ☀️ Instalación

+++

## Requisitos 

Necesitaremos:
  - Windows 7 o superior aunque se puede instalar en macOS y Linux
  - [git](https://git-scm.com/) para clonar este taller 
  - [nodejs](https://nodejs.org/es/)
    - Node v10+ para instalar las dependencias
    - Node v12 recomendado

* Para ver las versiones que tenemos instaladas   

```text
        git --version
        npm --version
        node --version
```

+++

## Crear un proyecto nuevo

Siguiendo estos pasos

```bash
cd c:\proyectos
mkdir cypress\primerproyectocypress
cd cypress\primerproyectocypress
npm init --yes
npm install -D cypress
```

+++

Esto creará un archivo package.json con las opciones por defecto y añadirá Cypress a las dependencias de desarrollo

```javascript
{
  "name": "primerproyectocypress",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
  "devDependencies": {
    "cypress": "^7.6.0"
  }
}

C:\proyectos\cypress\primerproyectocypress>npm install -D cypress
npm WARN deprecated har-validator@5.1.5: this library is no longer supported

Installing Cypress (version: 7.6.0)

|  Downloading Cypress      42% 476s
```

* Descargar Cypress cuesta unos minutos

+++

La versión de Cypress la consultamos con `node_modules\.bin\cypress version` y la correcta instalación con `node_modules\.bin\cypress verify `

```bash 
C:\proyectos\cypress\primerproyectocypress>node_modules\.bin\cypress version
Cypress package version: 7.6.0
Cypress binary version: 7.6.0
Electron version: 12.0.0-beta.14
Bundled Node version: 
14.15.1
C:\proyectos\cypress\primerproyectocypress>node_modules\.bin\cypress verify 
 ✔  Verified Cypress! C:\Users\javier\AppData\Local\Cypress\Cache\7.6.0\Cypress
```

Es importante que la versión sea superior a la 6.3

+++

### Como abrir Cypress

Podemos usar uno de estos dos comandos

```
npx Cypress open
node_modules\.bin\cypress open
```

Esto nos mostrará la pantalla siguiente

+++

<img src="./img/welcomeToCypress.png" alt="Bienvenido a Cypress!" width="80%"/>

+++

Cypress ha creado su estructura de directorios y creado unos ejemplos

* La estructura de directorios de Cypress
  - "cypress.json" - la configuración Cypress 
  - "cypress/integration" - las archivos de tests (specs)
    - "cypress/integration/1-getting-started" - ejemplos de iniciación 
    - "cypress/integration/2-advanced-examples" - ejemplos avanzados
  - "cypress/fixtures" - para suministrar datos de prueba
  - "cypress/plugins" - para extender Cypress
  - "cypress/support" - comandos compartidos, utilidades

+++

En el archivo `package.json` podemos añadir esta línea de código

```json
{
  "scripts": {
    "cy:open": "cypress open"
  }
}
```

Lo que nos permite usar `npm run cy:open` para abrir Cypress

+++

🏠 [inicio](?)
➡️  [primer test](?p=01-primer-test)