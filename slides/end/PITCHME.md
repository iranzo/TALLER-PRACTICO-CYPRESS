## Créditos

* Taller basado en [testing-workshop-cypress](https://github.com/cypress-io/testing-workshop-cypress.git) realizado por [Gleb Bahmutov](https://www.cypress.io/blog/author/gleb/)

+++

![me estas diciendo ...?](./img/Meme.jpg)

+++

## Para profundizar

* La propia web de [Cypress.io](https://www.cypress.io)
- [https://docs.cypress.io/](https://docs.cypress.io/)
- [https://github.com/cypress-io/cypress](https://github.com/cypress-io/cypress)
- [https://gitter.im/cypress-io/cypress](https://gitter.im/cypress-io/cypress)
- [https://www.cypress.io/support/](https://www.cypress.io/support/)
- [@Cypress_io](https://twitter.com/Cypress_io)

+++ 

⬅️ [test visual](?p=04-test-visual)
🏠 [inicio](?)