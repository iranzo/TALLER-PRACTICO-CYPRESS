# TALLER-PRACTICO-CYPRESS

![Version](https://img.shields.io/badge/version-1.0.1-blue.svg?cacheSeconds=2592000)
![Prerequisite](https://img.shields.io/badge/npm-%3E%3D10.0-blue.svg)

* un taller práctico de uso de [cypress](https://www.cypress.io/) como herramienta de apoyo a los desarrollos de los distintos programas realizados en el trabajo

## REPOSITORIO

* repositorio del [taller-practico-cypress en gitlab](https://gitlab.com/iranzo/taller-practico-cypress/src/master/)
  * importado del repositorio de [taller-practico-cypress en bitbucket](https://bitbucket.org/javieriranzo3/taller-practico-cypress/src/master/)

# INSTALACIÓN DEL TALLER

* para mostrar el taller se usan diapositivas generadas con [reveal.js](https://revealjs.com) mediante una herramienta de desarrollo frontend llamada [vite](https://vitejs.dev)
* primero hay que clonar el repositorio y luego instalar los paquetes node

```bash
git clone https://javieriranzo3@bitbucket.org/javieriranzo3/taller-practico-cypress.git
cd taller-practico-cypress
npn install
```

* para ver las diapositivas del taller
  * `npm run slides:dev`
* y ya se pueden ver las diapositivas en la url `localhost:3000`

* para generar las diapositivas del taller 
  * `npm run slides:build`
* en el directorio build están las diapositivas del taller montadas
  * las podemos ver con cualquier servidor web por ejemplo con 

```bash
cd build
http-server
C:\proyectos\cypress\taller-practico-cypress\dist>http-server 
Starting up http-server, serving ./
Available on:
  http://192.168.1.37:8080
  http://127.0.0.1:8080
Hit CTRL-C to stop the server
```

# DESPLIEGUE

* Si no queremos generar las diapositivas podemos verlas desplegadas en [Netlify](https://taller-practico-cypress.netlify.app/)
* el despliegue se hace desde la web [de Netlify](https://app.netlify.com/sites/taller-practico-cypress/deploys)

# CRÉDITOS

* basado en el taller realizado por el equipo de [Cypress' testing-workshop-cypress repo](https://github.com/cypress-io/testing-workshop-cypress)

# NOTAS DE DESARROLLO DEL PROYECTO

## PARTES

* instalación
  * requisitos
* primer test
* uso durante el desarrollo
  * caso práctico con la inclusión de un nuevo colectivo en EPACF contra localhost
* uso para hacer tet de la modificación realizada en pre producción
  * caso práctico con la inclusión de un nuevo colectivo en EPACF en preaplicaciones
  * hacer tet de la versión
  * login con geb

# TODO

* lanzar test con distintos navegadores
* usar distintos tamaños de dispositivo
* mismo test en distinto entorno
* [palabras traviesas](https://github.com/minimaxir/big-list-of-naughty-strings/blob/master/blns.json) 
* comprobar que todos los gestores tienen acceso
* crear logs personalizados de los test
* test visuales
* explorar las cookies
* cypress y CI
  * hacer un bat para el ci que tenemos
    * replace de la palabra Tomcat por WSlogic

# CHANGELOG

* El formato esta basado en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),

## [Unreleased]

## [1.0.1]

### Added

* Visual testing

## [1.0.0]

### Added

* versión inicial
